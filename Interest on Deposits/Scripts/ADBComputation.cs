﻿using Interest_on_Deposits.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interest_on_Deposits.Scripts
{
    public class ADBComputation
    {
        public ADBValues Computation(List<Models.ADBDetails> adbLists, String CutOffDate)
        {
            ADBValues adbDetails = new ADBValues();
            int i = 0;
            Decimal Balance = 0;

            foreach (ADBDetails item in adbLists)
            {
                if (i + 1 != adbLists.Count)
                {
                    TimeSpan dateDiff = Convert.ToDateTime(adbLists[i + 1].TransactionDate) - Convert.ToDateTime(adbLists[i].TransactionDate);
                    item.DateDiff = (dateDiff).Days;
                    Balance += item.Balance;

                    item.CurrentBalance = Balance * item.DateDiff;
                    adbDetails.TotalBalance += item.CurrentBalance;
                    adbDetails.TotalNoDays += item.DateDiff;
                    i++;
                }
                else
                {
                    TimeSpan dateDiff = Convert.ToDateTime(CutOffDate) - Convert.ToDateTime(adbLists[i].TransactionDate);
                    item.DateDiff = (dateDiff).Days;
                    Balance += item.Balance;

                    item.CurrentBalance = Balance * item.DateDiff;

                    adbDetails.TotalBalance += item.CurrentBalance;
                    adbDetails.TotalNoDays += item.DateDiff;
                }
            }

            return adbDetails;
        }
    }
}
