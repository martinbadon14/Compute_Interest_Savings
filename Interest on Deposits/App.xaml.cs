﻿using CRUXLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Interest_on_Deposits
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string[] args;
        public static API CRUXAPI = new API();
        protected override void OnStartup(StartupEventArgs e)
        {
            App.args = e.Args;
            List<String> parameters = App.args.ToList<String>();
            try
            {
                CRUXAPI.loadParams(parameters);
            }
            catch (Exception ex) { }
        }
    }
}
