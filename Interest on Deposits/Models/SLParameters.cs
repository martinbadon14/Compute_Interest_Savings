﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interest_on_Deposits.Models
{
    public class SLParameters
    {
        public Int16 BranchCode { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public Byte SLN { get; set; }
        public String SLDesc { get; set; }
        public Int64 ClientID { get; set; }
        public String DateTo { get; set; }
        public String DateFrom { get; set; }
        public String CutOffDate { get; set; }
        public String RefNo { get; set; }
        public Decimal InterestRate { get; set; }
    }

    public class ADBDetails
    {
        public Decimal Balance { get; set; }
        public String TransactionDate { get; set; }
        public Int32 DateDiff { get; set; }
        public Decimal CurrentBalance { get; set; }
    }

    public class ADBValues
    {
        public Decimal TotalBalance { get; set; }
        public int TotalNoDays { get; set; }
    }
}
