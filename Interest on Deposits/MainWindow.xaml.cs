﻿using Interest_on_Deposits.Models;
using Interest_on_Deposits.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interest_on_Deposits
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string apiCrux;
        private SLParameters slParams;

        public MainWindow()
        {
            InitializeComponent();
            slParams = new SLParameters();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            apiCrux = App.CRUXAPI.Arguments;

            if (!String.IsNullOrEmpty(apiCrux))
            {
                slParams = new JavaScriptSerializer().Deserialize<SLParameters>(apiCrux);
                GetADB(slParams);
            }
            else
            {
                slParams = new SLParameters();
                slParams.BranchCode = 5;
                slParams.ClientID = 22084;
                slParams.SLC = 22;
                slParams.SLT = 5;
                slParams.SLE = 11;
                slParams.SLN = 15;
                slParams.RefNo = "763";
                GetADB(slParams);
                //slParams.SLDesc = "Savings - Regular";
            }

            this.DataContext = slParams;
        }


        private void GetADB(SLParameters slParameters)
        {
            String parsed = new JavaScriptSerializer().Serialize(slParameters);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/searchSLType", parsed);

            if (Response.Status == "SUCCESS")
            {
                Models.SLParameters slDetails = new JavaScriptSerializer().Deserialize<Models.SLParameters>(Response.Content);
                slParams.SLDesc = slDetails.SLDesc;
            }
            else
            {
                MessageBox.Show("Failed to Retrieve Data!");
            }
        }


        private void Funtion()
        {
            slParams.DateFrom = FromDate.Text;
            slParams.CutOffDate = CutOffDate.Text;

            String parsed = new JavaScriptSerializer().Serialize(slParams);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/ADB", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<Models.ADBDetails> adbLists = new JavaScriptSerializer().Deserialize<List<Models.ADBDetails>>(Response.Content);
                Decimal ADBValue = 0;

                ADBComputation adbComp = new ADBComputation();

                ADBValues adbValues = adbComp.Computation(adbLists, CutOffDate.Text);

                ADBValue = adbValues.TotalBalance / adbValues.TotalNoDays;
                ADBBalance.Text = String.Format("{0:N2}", ADBValue);

            }
            else
            {
                MessageBox.Show("Failed to Retrieve Data! Please Contact Administrator");
            }

        }


        private void ComputeInterest()
        {
            Decimal IntAmount = Convert.ToDecimal(ADBBalance.Text) * (Convert.ToDecimal(IntRateTextBox.Text) / 100);
            Decimal noDays = DateTime.DaysInMonth(Convert.ToDateTime(CutOffDate.Text).Year, Convert.ToDateTime(CutOffDate.Text).Month);

            var InterestAmt = IntAmount * (noDays / 360);
            Decimal GrossInterestAmt = Convert.ToDecimal(InterestAmt.ToString("N7", CultureInfo.CreateSpecificCulture("en-US")));
            Decimal NetInterestAmt = Convert.ToDecimal(GrossInterestAmt.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));

            InterestAmountTextBox.Text = String.Format("{0:#,##0.00}", NetInterestAmt);
        }


        private void ToFromDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            String name = (sender as TextBox).Name.ToString();

            if (e.Key == Key.Tab)
            {
                try
                {
                    if (name == "FromDate")
                    {
                        DateTime.Parse(FromDate.Text);
                    }
                    else if (name == "ToDate")
                    {
                        DateTime.Parse(ToDate.Text);
                        OkButton.IsEnabled = true;
                    }
                    else if (name == "CutOffDate")
                    {
                        DateTime.Parse(CutOffDate.Text);
                    }
                }
                catch
                {
                    e.Handled = true;
                    MessageBox.Show("Please input correct Date.");
                }
            }
            else if (e.Key == Key.Enter)
            {
                try
                {
                    if (name == "ToDate")
                    {
                        if (CutOffDate.Text != "__/__/____")
                        {
                            Funtion();
                        }
                        else
                        {
                            MessageBox.Show("CutOff Date Required!");
                        }
                    }
                    else if (name == "CutOffDate")
                    {
                        if (!String.IsNullOrEmpty(CutOffDate.Text) &&
                            !String.IsNullOrEmpty(FromDate.Text) &&
                            !String.IsNullOrEmpty(ToDate.Text))
                        {
                            Funtion();
                        }
                    }
                    else if (name == "IntRateTextBox")
                    {
                        if (!String.IsNullOrEmpty(CutOffDate.Text) &&
                            !String.IsNullOrEmpty(FromDate.Text) &&
                            !String.IsNullOrEmpty(ToDate.Text) && !String.IsNullOrEmpty(IntRateTextBox.Text))
                        {
                            if (String.IsNullOrEmpty(ADBBalance.Text))
                            {
                                Funtion();
                            }

                            ComputeInterest();
                        }
                    }


                }
                catch (Exception)
                {
                    e.Handled = true;
                }

            }
        }


        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(CutOffDate.Text) &&
                            !String.IsNullOrEmpty(FromDate.Text) &&
                            !String.IsNullOrEmpty(ToDate.Text) && String.IsNullOrEmpty(IntRateTextBox.Text))
                {
                    Funtion();
                }
                else if (!String.IsNullOrEmpty(CutOffDate.Text) &&
                            !String.IsNullOrEmpty(FromDate.Text) &&
                            !String.IsNullOrEmpty(ToDate.Text) && !String.IsNullOrEmpty(IntRateTextBox.Text))
                {
                    Funtion();
                    ComputeInterest();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to Exit this Module?", "Close", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        this.Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }
            }
        }


        private void FromToDate_GotFocus(object sender, RoutedEventArgs e)
        {
            String textName = (sender as TextBox).Name.ToString();

            if (textName == "FromDate")
            {
                if (FromDate.IsFocused)
                {
                    FromDate.Background = System.Windows.Media.Brushes.Yellow; // WPF
                    FromDate.IsInactiveSelectionHighlightEnabled = true;
                }
            }
            else if (textName == "ToDate")
            {
                if (ToDate.IsFocused)
                {
                    ToDate.Background = System.Windows.Media.Brushes.Yellow; // WPF
                    ToDate.IsInactiveSelectionHighlightEnabled = true;
                }
            }
            else if (textName == "CutOffDate")
            {
                if (CutOffDate.IsFocused)
                {
                    CutOffDate.Background = System.Windows.Media.Brushes.Yellow; // WPF
                    CutOffDate.IsInactiveSelectionHighlightEnabled = true;
                }
            }
            else if (textName == "IntRateTextBox")
            {
                IntRateTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                IntRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void FromToDate_LostFocus(object sender, RoutedEventArgs e)
        {
            String textName = (sender as TextBox).Name.ToString();

            if (textName == "FromDate")
            {

                FromDate.Background = System.Windows.Media.Brushes.White; // WPF
                FromDate.IsInactiveSelectionHighlightEnabled = true;

            }
            else if (textName == "ToDate")
            {

                ToDate.Background = System.Windows.Media.Brushes.White; // WPF
                ToDate.IsInactiveSelectionHighlightEnabled = true;

                CutOffDate.Text = ToDate.Text;

            }
            else if (textName == "CutOffDate")
            {

                CutOffDate.Background = System.Windows.Media.Brushes.White; // WPF
                CutOffDate.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "IntRateTextBox")
            {
                IntRateTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                IntRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FromDate.Focus();
        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal value1;
            if (decimal.TryParse(((TextBox)sender).Text, out value1))
            {
                string[] a = ((TextBox)sender).Text.Split(new char[] { '.' });
                int decimals = 0;
                if (a.Length == 2)
                {
                    decimals = a[1].Length;
                }
                else
                {
                    string newString = Regex.Replace(((TextBox)sender).Text, "[^0-9]", "");
                    ((TextBox)sender).Text = string.Format("{0:#,##0}", Double.Parse(newString));
                }
                if (decimals > 2)
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
                            ((TextBox)sender).Focus();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
            }
            else
            {
                if (((TextBox)sender).Text != "")
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
            }
        }
    }
}
